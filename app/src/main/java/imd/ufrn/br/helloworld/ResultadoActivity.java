package imd.ufrn.br.helloworld;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ResultadoActivity extends Activity {

    private TextView resultadoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        resultadoTextView = (TextView) findViewById(R.id.msgTextView);
        String msgRecebida = getIntent().getStringExtra("msg");
        resultadoTextView.setText(msgRecebida);
    }


}
